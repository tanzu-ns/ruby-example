# README

Steps if modifying the code locally
- Clone the repo
- Install Ruby & `gem install rails`
- Add credentials via `EDITOR="vim" rails credentials:edit`, an example is below
- Generate a secret with `RAILS_ENV=production rake secret`
- This will create a new master key used below

Example credentials
```yaml
development:
  secret_key_base: abcdef

production:
  secret_key_base: 1234abcd
```

In order to deploy this to TAP run the following:
```bash
tanzu app workload apply -f tap/workload.yml \
--env "RAILS_MASTER_KEY=abcdef"\
 --env "SECRET_KEY_BASE=abcdef"\
 --build-env "RAILS_MASTER_KEY=abcdef" \
 --build-env "SECRET_KEY_BASE=abcdef"
```
